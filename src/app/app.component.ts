import {Component, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('form') ngForm: NgForm;

  defaultSubscription = 'Advanced';

  formSubmitted = {
    email: '',
    subscription: '',
    password: ''
  };

  submitted = false;

  onSubmit() {
    this.submitted = true;
    this.formSubmitted.email = this.ngForm.value.email;
    this.formSubmitted.subscription = this.ngForm.value.subscription;
    this.formSubmitted.password = this.ngForm.value.password;
  }

}
